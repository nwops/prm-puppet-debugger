FROM puppet/puppet-agent:7.11.0
RUN /opt/puppetlabs/puppet/bin/gem install bundler --no-document
RUN /opt/puppetlabs/puppet/bin/gem install puppet-debugger -f --conservative --minimal-deps -N
COPY ./content/* /tmp/ 
VOLUME [ /code, /cache ]
WORKDIR /code
ENTRYPOINT [ "/opt/puppetlabs/bin/puppet", "debugger" ]
